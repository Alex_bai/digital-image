/**
 * Function Description:
 * An operation that creates an ARGB image that is a copy of the source except 
 * for the alpha channel which is arranged in either a radial or gradient pattern.
 */
package Assignment2;

import java.awt.Color;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorModel;

import pixeljelly.ops.PluggableImageOp;

public class AlphaMaskOp implements PluggableImageOp {

	private int type;
	
	public AlphaMaskOp(int type)
	{
		this.type = type;
	}
	
	public AlphaMaskOp()
	{
		this.getDefault();
	}
	
	//Constructs a filter that is of type 3.
	public void getDefault()
	{
		this.type = 3;
	}
	
	@Override
	public BufferedImage createCompatibleDestImage(BufferedImage src,
			ColorModel destCM) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BufferedImage filter(BufferedImage src, BufferedImage dest) {

		int alpha = 0;
		//if the destination is null, create a new BufferedImage, and the type is ARGB
		if(dest == null)
		{
			dest = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_ARGB);
		}
		
		for(int row=0; row<src.getHeight(); row++)
		{
			for(int col=0; col<src.getWidth(); col++)
			{
				//get the value of alpha
				switch(type)
				{
				//when the type is 1, a linear alpha channel transparent at the top and opaque at the bottom.
				case 1:
					alpha = row*255/src.getHeight();
					break;
					
				//when the type is -1, a linear alpha channel transparent at the bottom and opaque at the top
				case -1:
					alpha = (src.getHeight()-row)*255/src.getHeight();
					break;
					
				//when the type is 2, a linear alpha channel transparent at the left and opaque on the right. 
				case 2:
					alpha = col*255/src.getWidth();
					break;
					
				//when the type is -2, a linear alpha channel transparent at the right and opaque on the left.
				case -2:
					alpha = (src.getWidth()-col)*255/src.getWidth();
					break;
					
				//when the type is 3, a radial alpha channel transparent in the center and opaque at the corners.
				case 3:
					//the locationXandY is the square value of (col,row) to (width/2,height/2)
					double locationXandY = Math.pow(src.getWidth()/2-col,2)+Math.pow(src.getHeight()/2-row, 2);
					
					//the lengthToCenter is the square value of (0,0) to (width/2,height/2)
					double lengthToCenter = Math.pow(src.getWidth()/2, 2)+Math.pow(src.getHeight()/2, 2);
					
					alpha = (int) (Math.pow(locationXandY/lengthToCenter, 0.5)*255);
					break;
					
				//when the type is -3, a radial alpha channel opaque in the center and transparent at the corners.
				case -3:
					//the locationXandY is the square value of (col,row) to (width/2,height/2)
					double locationXandY1 = Math.pow(src.getWidth()/2-col,2)+Math.pow(src.getHeight()/2-row, 2);
					
					//the lengthToCenter is the square value of (0,0) to (width/2,height/2)
					double lengthToCenter1 = Math.pow(src.getWidth()/2, 2)+Math.pow(src.getHeight()/2, 2);
					
					alpha = 255 - (int) (Math.pow(locationXandY1/lengthToCenter1, 0.5)*255);
					break;
				}
				
				//get the value of rgb on the src image
				int rgb = src.getRGB(col, row);
				
				//get the color of rgb
				Color color = new Color(rgb);
				
				//get the color which added alpha
				color = new Color(color.getRed(),color.getGreen(),color.getBlue(),alpha);
				
				//get the result rgb which added alpha
				rgb = color.getRGB();
				
				//set the color of rgb to dest image
				dest.setRGB(col, row, rgb);	
			}
		}
		
		return dest;
	}

	@Override
	public Rectangle2D getBounds2D(BufferedImage src) {
		// TODO Auto-generated method stub
		return src.getRaster().getBounds();
	}

	@Override
	public Point2D getPoint2D(Point2D srcPt, Point2D dstPt) {
		// TODO Auto-generated method stub
		if(dstPt == null)
		{
			dstPt = (Point2D) srcPt.clone();
		}else{
			dstPt.setLocation(srcPt);
		}
		return dstPt;
	}

	@Override
	public RenderingHints getRenderingHints() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAuthorName() {
		// TODO Auto-generated method stub
		return "Chenguang Bai";
	}

	@Override
	public BufferedImageOp getDefault(BufferedImage arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
