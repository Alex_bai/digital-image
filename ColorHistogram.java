package Exam;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import pixeljelly.scanners.Location;
import pixeljelly.scanners.RasterScanner;

public class ColorHistogram {

	private int Rn;
	private int Gn;
	private int Bn;
	
	public ColorHistogram(int Rn, int Gn, int Bn)
	{
		this.Rn = Rn;
		this.Gn = Gn;
		this.Bn = Bn;
	}
	
	public float[] getHistogram(BufferedImage br) 
	{
		int [] histogram = new int[Rn*Gn*Bn];
		float result[] = new float[Rn*Gn*Bn];
		
    	for(Location pt : new RasterScanner(br, true))
		{
			//get the RGB color of source image
			int rgb = br.getRGB(pt.col, pt.row);
			Color color = new Color(rgb);
			
			int red = color.getRed();
			int green = color.getGreen();
			int blue = color.getBlue();
			
			int f_red = red*Rn/256;
			int f_green = green*Gn/256;
			int f_blue = blue*Bn/256;
			
			//R’*(Nr*Nb) + G’*(Ng) + B’.
			int index = f_red*(Gn*Bn) + f_green*Bn + f_blue;
			
			histogram[index]++;
		}

    	//normalize the histogram
    	for(int i=0; i<result.length; i++)
    	{
    		result[i] = histogram[i]/(float)(br.getWidth()*br.getHeight());
    	}
    	
    	return result;
	}
	
	public int[] getCenterColor(BufferedImage br)
	{
		int result[] = new int[Rn*Gn*Bn];
		
		for(Location pt : new RasterScanner(br, true))
		{
			//get the RGB color of source image
			int rgb = br.getRGB(pt.col, pt.row);
			Color color = new Color(rgb);
			
			int red = color.getRed();
			int green = color.getGreen();
			int blue = color.getBlue();
			
			int f_red = red*Rn/256;
			int f_green = green*Gn/256;
			int f_blue = blue*Bn/256;
			
			int Rc = f_red*(256/Rn) + 128/Rn;
			int Gc = f_green*(256/Gn) + 128/Gn;
			int Bc = f_blue*(256/Bn) + 128/Bn;
			
			int index = f_red*(Gn*Bn) + f_green*Bn + f_blue;
			
			color = new Color(Rc,Gc,Bc);
			
			result[index] = color.getRGB();
		}
		return result;
		
	}
	
	public float getDistance(int rgb1, int rgb2)
	{
		Color color1 = new Color(rgb1);
		Color color2 = new Color(rgb2);
		
		int red1 = color1.getRed();
		int green1 = color1.getGreen();
		int blue1 = color1.getBlue();
		
		int red2 = color2.getRed();
		int green2 = color2.getGreen();
		int blue2 = color2.getBlue();
		
		float distance = (float) Math.pow(Math.pow(red1-red2, 2) + Math.pow(green1-green2,2) +
				Math.pow(blue1-blue2, 2),0.5);
		
		return distance;
//		
	}
	
//	public float[][][] getHSB(BufferedImage br)
//	{
//		float[][][] hsb = new float[br.getHeight()][br.getWidth()][3];
//		for(Location pt : new RasterScanner(br, true))
//		{
//			int rgb = br.getRGB(pt.col, pt.row);
//			Color color = new Color(rgb);
//			int red = color.getRed();
//			int green = color.getGreen();
//			int blue = color.getBlue();
//			hsb[]
//		}
//	}
	
//	public void getColorDistance(int i,int j)
//	{
//		for()
//	}
}
