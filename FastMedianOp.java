package Assignment3;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.WritableRaster;

import pixeljelly.ops.NullOp;
import pixeljelly.ops.PluggableImageOp;
import pixeljelly.scanners.Location;
import pixeljelly.scanners.RasterScanner;
import pixeljelly.utilities.ImagePadder;
import pixeljelly.utilities.Mask;
import pixeljelly.utilities.ReflectivePadder;

public class FastMedianOp extends NullOp implements PluggableImageOp {

	private int m,n;
	private ImagePadder padder = ReflectivePadder.getInstance(); 
	private int index;
	private int sum;
	private int midSum;
	
	public FastMedianOp(int m, int n)
	{
		setWidth(m);
		setRow(n);
	}
	
	public FastMedianOp()
	{
		setWidth(5);
		setRow(5);
	}
	
	public int getWidth() {
		return m;
	}

	public void setWidth(int m) {
		this.m = m;
	}

	public int getRow() {
		return n;
	}

	public void setRow(int n) {
		this.n = n;
	}

	public BufferedImage filter(BufferedImage src, BufferedImage dest)
	{
		if(dest == null)
		{
			dest = createCompatibleDestImage(src,src.getColorModel());
		}
		//get the index of median number
		midSum = (m*n+1)/2;
		WritableRaster raster = src.getRaster();
		boolean flag[] = new boolean[m*n];
		for(int i=0;i<flag.length;i++)
		{
			flag[i] = true;
		}
		//initial the Mask
		Mask mask = new Mask(m,n,flag);
		for(int band=0;band<raster.getNumBands();band++)
		{
			for(int row=0;row<src.getHeight();row++)
			{
				//initial the median index and the value of sum samples
				index = -1;
				sum = 0;
				//get the initial histogram of the first sample in each row
				int histogram[] = getInitialHistogram(src,row,0,band,mask);
				//get the median index of the histogram
				getIndex(histogram);
				dest.getRaster().setSample(0, row, band, index);
				for(int col=1;col<src.getWidth();col++)
				{
					//get the histogram of the sample through the previous histogram
					histogram = getHistogramR(src,row,col,band,histogram);
					//get the median index of the histogram
					getIndex(histogram);
					dest.getRaster().setSample(col, row, band, index);
				}
			}
		}
		return dest;
	}
	
	//get the initial histogram of the first sample in each row
	public int[] getInitialHistogram(BufferedImage src, int row, int col, int band, Mask mask)
	{
		int histogram[] = new int[256];
		for(Location mPt : new RasterScanner(mask.getBounds()))
		{
			int sample = padder.getSample(src, col+mPt.col, row+mPt.row,band);
			histogram[sample]++;
		}
		return histogram;
	}
	
	//get the histogram of the sample through the previous histogram
	public int[] getHistogramR(BufferedImage src, int row, int col, int band, int histogramR[])
	{
		for(int i=row-n/2;i<=row+n/2;i++)
		{
			int sample1 = padder.getSample(src,col-(m+1)/2,i,band);
			int sample2 = padder.getSample(src,col+m/2,i,band);
			histogramR[sample1]--;
			histogramR[sample2]++;
			//get the value of sum samples before the median index
			if(index>=sample1)
			{
				sum--;
			}
			if(index>=sample2)
			{
				sum++;
			}
		}
		return histogramR;
	}
	
	//get the median index in the histogram
	public void getIndex(int res[])
	{
		if(sum<midSum)
		{
			while(sum<midSum)
			{
				sum += res[++index];
			}
		}else if(sum>midSum)
		{
			while((sum-res[index])>=midSum)
			{
				sum -= res[index--];
			}
		}
	}
	
	public String getAuthorName() {
		return "Chenguang Bai";
	}

	public BufferedImageOp getDefault(BufferedImage src) {
		return new FastMedianOp();
	}
}
