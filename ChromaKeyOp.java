package Exam;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorModel;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import pixeljelly.ops.NullOp;
import pixeljelly.ops.PluggableImageOp;
import pixeljelly.utilities.ImagingUtilities;

public class ChromaKeyOp extends NullOp implements PluggableImageOp {

	private BufferedImage back;
	private Color k;
	private double dmin;
	private double dmax;
	private boolean metric;
	private String cspace;
	
	public ChromaKeyOp()
	{
		this.getDefault();
	}
	
	public ChromaKeyOp(BufferedImage back, Color k, double dmin, double dmax, boolean metric, String cspace)
	{
		this.back = back;
		this.k = k;
		this.dmin = dmin;
		this.dmax = dmax;
		this.metric = metric;
		this.cspace = cspace;
	}
	
	public BufferedImage filter(BufferedImage src, BufferedImage dest)
	{
		if(dest == null)
		{
			dest = createCompatibleDestImage(src,src.getColorModel());
		}
		
		double alpha = 0;
		
		//get the pixel of color k
		float []destColor = getPixel(k);
		
		for(int row=0; row<Math.min(src.getHeight(), back.getHeight()); row++)
		{
			for(int col=0; col<Math.min(src.getWidth(), back.getWidth()); col++)
			{
				int rgb = src.getRGB(col, row);
				
				Color color = new Color(rgb);
				
				//get the pixels for these two colors
				float []pixel = getPixel(color);
				
				//get the distance between the two colors
				double distance = getDistance(pixel, destColor);
				
				//get the value of alpha
				if(distance < dmin)
				{
					alpha = 0;
				}else if(distance >= dmin && distance <= dmax)
				{
					alpha = (distance-dmin)/(dmax-dmin);
				}else{
					alpha = 1;
				}
				
				//the rgb color of background image
				int back_rgb = back.getRGB(col, row);
				Color back_color = new Color(back_rgb);
				
				int result[] = new int[3];
				result[0] = (int) (alpha * color.getRed() + (1-alpha) * back_color.getRed());
				result[1] = (int) (alpha * color.getGreen() + (1-alpha) * back_color.getGreen());
				result[2] = (int) (alpha * color.getBlue() + (1-alpha) * back_color.getBlue());
				
				rgb = new Color(result[0], result[1], result[2]).getRGB();
				dest.setRGB(col, row, rgb);
			}
		}
		
		return dest;
	}
	
	public float[] getPixel(Color color)
	{
		//initial a float array to put the end value
		float pixel[] = new float[3];
		
		int red = color.getRed();
		int green = color.getGreen();
		int blue = color.getBlue();
		
		if(cspace.equals("yiq"))
		{
			//convert RGB to YIQ
			pixel = this.RGBtoYIQ(red, green, blue);
		}else if(cspace.equals("hsb"))
		{
			//convert RGB to HSB
			pixel = Color.RGBtoHSB(red, green, blue, null);
		}else if(cspace.equals("rgb")){
			pixel[0] = red/255f;
			pixel[1] = green/255f;
			pixel[2] = blue/255f;
		}
		return pixel;
	}
	
	
	public float[] RGBtoYIQ(int red, int green, int blue)
	{
		float r = red/255f;
		float g = green/255f;
		float b = blue/255f;
		
		float Y = 0.299f*r + 0.587f*g + 0.114f*b;
		float I = 0.596f*r - 0.274f*g - 0.321f*b;
		float Q = 0.211f*r - 0.523f*g + 0.312f*b;
		
		float yiq[] = {Y,I,Q};
		
		return yiq;
	}
	
	/**
	 * get the normalized distance
	 * @param pixel
	 * @param destColor
	 * @return
	 */
	public double getDistance(float pixel[], float destColor[])
	{
		double distance = 0;
		
		if(cspace.equals("rgb"))
		{
			//if metric is true, and the distance between two colors would be calculated in L2 method
			if(metric)
			{
				double result = Math.pow(pixel[0]-destColor[0],2)+
						Math.pow(pixel[1]-destColor[1],2)+
						Math.pow(pixel[2]-destColor[2], 2);

				distance = Math.pow(result, 0.5)/Math.pow(3, 0.5);
			}else{		//if metric is false, and the distance between two colors would be calculated in L1 method
				
				double result = Math.abs(pixel[0]-destColor[0])+
						Math.abs(pixel[1]-destColor[1])+
						Math.abs(pixel[2]-destColor[2]);
				distance = result/3;
			}
		}else if(cspace.equals("yiq"))
		{
			if(metric)
			{
				double result = Math.pow(pixel[0]-destColor[0],2)+
						Math.pow(pixel[1]-destColor[1],2)+
						Math.pow(pixel[2]-destColor[2], 2);

				distance = Math.pow(result, 0.5)/Math.pow(1+2*0.5957f+0.5226f, 0.5);
			}else{		
				
				double result = Math.abs(pixel[0]-destColor[0])+
						Math.abs(pixel[1]-destColor[1])+
						Math.abs(pixel[2]-destColor[2]);
				distance = result/(1+0.5957f+0.5226f);
			}
		}else if(cspace.equals("hsb"))
		{
			double pi = Math.PI;
			
			double a1 = pixel[1]*Math.cos(2*pi*pixel[0]);
			double a2 = destColor[1]*Math.cos(2*pi*destColor[0]);
			
			double b1 = pixel[1]*Math.sin(2*pi*pixel[0]);
			double b2 = destColor[1]*Math.sin(2*pi*destColor[0]);
			
			double c1 = pixel[2];
			double c2 = destColor[2];
			
			if(metric)
			{
				double result = Math.pow(a1-a2, 2)+Math.pow(b1-b2, 2)+Math.pow(c1-c2, 2);
				
				distance = Math.pow(result, 0.5)/Math.pow(5, 0.5);
			}else{		//if metric is false, and the distance between two colors would be calculated in L1 method
				
				double result = Math.abs(a1-a2) + Math.abs(b1-b2) + Math.abs(c1-c2);
				distance = result/(1+2*Math.pow(2, 0.5));
			}
		}
		
		return distance;
	}
	
	public BufferedImage getBack() {
		return back;
	}

	public void setBack(BufferedImage back) {
		this.back = back;
	}

	public Color getK() {
		return k;
	}

	public void setK(Color k) {
		this.k = k;
	}

	public double getDmin() {
		return dmin;
	}

	public void setDmin(double dmin) {
		this.dmin = dmin;
	}

	public double getDmax() {
		return dmax;
	}

	public void setDmax(double dmax) {
		this.dmax = dmax;
	}

	public boolean isMetric() {
		return metric;
	}

	public void setMetric(boolean metric) {
		this.metric = metric;
	}

	public String getCspace() {
		return cspace;
	}

	public void setCspace(String cspace) {
		this.cspace = cspace;
	}

	@Override
	public String getAuthorName() {
		// TODO Auto-generated method stub
		return "Chenguang Bai";
	}

	public void getDefault()
	{
		try {
			this.back = ImageIO.read(new File
					("/Users/chenguangbai/Documents/Digital Image Processing/Test Source/bandExtract.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.k = new Color(30,220,110);
		this.dmin = 0.16;
		this.dmax = 0.25;
		this.metric = false;
		this.cspace = "RGB";
	}
	
	@Override
	public BufferedImageOp getDefault(BufferedImage arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
