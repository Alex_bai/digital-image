package Assignment3;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;

import pixeljelly.ops.NullOp;
import pixeljelly.ops.PluggableImageOp;
import pixeljelly.scanners.Location;
import pixeljelly.scanners.RasterScanner;

public class SaltAndPepperOp extends NullOp implements PluggableImageOp {
	
	private double prob;
	
	public SaltAndPepperOp(double prob)
	{
		setProb(prob);
	}

	public SaltAndPepperOp()
	{
		setProb(0.3);
	}
	
	public double getProb() {
		return prob;
	}

	public void setProb(double prob) {
		this.prob = prob;
	}

	public BufferedImage filter(BufferedImage src, BufferedImage dest)
	{
		if(dest == null)
		{
			dest = createCompatibleDestImage(src, src.getColorModel());
		}
		for(Location pt : new RasterScanner(src,true))
		{
			//get the random value and the range is from 0 to 1
			double probability = Math.random();
			//if the random value is less than prob, produce the noise sample 
			//and put the sample to the destination image
			if(probability < prob)
			{
				//get the random value and the range is from  0 to 2,
				//because the type is int, so there are only two values 0 and 1
				int noise = (int) (Math.random()*2);
				int sample = 0;
				//if the value is 0, and put 0 to the sample
				if(noise == 0)
				{
					sample = 0;
				}else if(noise == 1)	//if the value is 1, and 255 to the sample
				{
					sample = 255;
				}
				dest.getRaster().setSample(pt.col, pt.row, pt.band, sample);
			}else{	//if the random value is not less than prob, then set the source sample to the destination image
				int sample = src.getRaster().getSample(pt.col,pt.row,pt.band);
				dest.getRaster().setSample(pt.col, pt.row, pt.band, sample);
			}
		}
		return dest;
	}
	
	public String getAuthorName() {
		return "Chenguang Bai";
	}

	public BufferedImageOp getDefault(BufferedImage arg0) {
		return new SaltAndPepperOp();
	}

}
