package Assignment4;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;

import pixeljelly.ops.NullOp;
import pixeljelly.ops.PluggableImageOp;
import pixeljelly.utilities.ImagePadder;
import pixeljelly.utilities.ReflectivePadder;

public class KaliedoscopeGeometricOp extends NullOp implements PluggableImageOp {

	private int angle;
	private int numSlices;
	
	public KaliedoscopeGeometricOp(int angle,int numSlices)
	{
		setAngle(angle);
		setNumSlices(numSlices);
	}
	
	public KaliedoscopeGeometricOp()
	{
		setAngle(30);
		setNumSlices(8);
	}
	
	public int getAngle() {
		return angle;
	}

	public void setAngle(int angle) {
		this.angle = angle;
	}

	public int getNumSlices() {
		return numSlices;
	}

	public void setNumSlices(int numSlices) {
		this.numSlices = numSlices;
	}

	public BufferedImage filter(BufferedImage src, BufferedImage dest)
	{
		ImagePadder padder = ReflectivePadder.getInstance();
		
		if(dest == null)
		{
			dest = createCompatibleDestImage(src, src.getColorModel());
		}
		//get the value of width and height for the source image
		int width = src.getWidth();
		int height = src.getHeight();
		double pi = Math.PI;
		//get the range angle of each region
		double rangeAngle = 2*pi/numSlices; 
		for(int row=0;row<height;row++)
		{
			for(int col=0;col<width;col++)
			{
				//the coordinate given in polar coordinates with respect to the 
				//center of the source image
				int x = height/2-row;
				int y = col-width/2;
				//get the value of strength
				double strength = Math.sqrt(x*x+y*y);
				//the angle of the point
				double data = Math.atan2(x, y);
				//change the polar coordinate to clockwise, and north is 0, the range is 0 to 360
				if(data>=0 && data<=pi/2)
				{
					data = pi/2-data;
				}else if(data>=pi/2 && data<=pi)
				{
					data = (double)5/2*pi-data;
				}else if(data<0 && data>=(-pi))
				{
					data = pi/2-data;
				}
				//get the begin angle and end angle
				double begin = (double)angle/180*pi-rangeAngle/2;
				double end = (double)angle/180*pi+rangeAngle/2;
				//get the value of n, and n decide the reflected
				int n = 0;	
				if(data > end)
				{
					while((data-rangeAngle) > begin)
					{
						data -= rangeAngle;
						n++;
					}
				}else{
					data += 2*pi;
					while((data-rangeAngle) > begin)
					{
						data -= rangeAngle;
						n++;
					}
				}
				//get the corresponds location(row and column)
				if(n%2 == 1)
				{
					data = end-data+begin;
					if(data>=0 && data<=pi/2)
					{
						x = height/2 - (int)(strength*Math.cos(data));
						y = width/2 + (int)(strength*Math.sin(data));
					}else if(data>pi/2 && data<=pi)
					{
						x = height/2 + (int)(strength*Math.cos(pi-data));
						y = width/2 + (int)(strength*Math.sin(pi-data));
					}else if(data>pi && data<=(double)3/2*pi)
					{
						x = height/2 + (int)(strength*Math.cos(data-pi));
						y = width/2 - (int)(strength*Math.sin(data-pi));
					}else{
						x = height/2 - (int)(strength*Math.cos(2*pi-data));
						y = width/2 - (int)(strength*Math.sin(2*pi-data));
					}
				}else{
					if(data>=0 && data<=pi/2)
					{
						x = height/2 - (int)(strength*Math.cos(data));
						y = width/2 + (int)(strength*Math.sin(data));
					}else if(data>pi/2 && data<=pi)
					{
						x = height/2 + (int)(strength*Math.cos(pi-data));
						y = width/2 + (int)(strength*Math.sin(pi-data));
					}else if(data>pi && data<=(double)3/2*pi)
					{
						x = height/2 + (int)(strength*Math.cos(data-pi));
						y = width/2 - (int)(strength*Math.sin(data-pi));
					}else{
						x = height/2 - (int)(strength*Math.cos(2*pi-data));
						y = width/2 - (int)(strength*Math.sin(2*pi-data));
					}
				}
				//get the corresponds samples in the source image
				int sample0 = padder.getSample(src, y, x, 0);
				int sample1 = padder.getSample(src, y, x, 1);
				int sample2 = padder.getSample(src, y, x, 2);
				//put the samples to destination image
				dest.getRaster().setSample(col, row, 0, sample0);
				dest.getRaster().setSample(col, row, 1, sample1);
				dest.getRaster().setSample(col, row, 2, sample2);
			}
		}	
		return dest;
	}

	public String getAuthorName() {
		return "Chenguang Bai";
	}

	public BufferedImageOp getDefault(BufferedImage arg0) {
		return new KaliedoscopeGeometricOp();
	}
}
