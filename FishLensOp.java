package Assignment4;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;

import pixeljelly.ops.NullOp;
import pixeljelly.ops.PluggableImageOp;
import pixeljelly.utilities.ImagePadder;
import pixeljelly.utilities.ReflectivePadder;

public class FishLensOp extends NullOp implements PluggableImageOp {

	private double weight;
	private boolean flag;
	
	public FishLensOp(double weight, boolean flag)
	{
		setWeight(weight);
		setFlag(flag);
	}
	
	public FishLensOp()
	{
		setWeight(5);
		setFlag(false);
	}
	
	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	public BufferedImage filter(BufferedImage src, BufferedImage dest)
	{
		if(dest == null)
		{
			dest = createCompatibleDestImage(src, src.getColorModel());
		}
		ImagePadder padder = ReflectivePadder.getInstance();
		//get the value of focalLength and scale
		int focalLength = Math.max(src.getWidth(), src.getHeight())/2;
		double scale = focalLength / (Math.log(weight*focalLength+1));
		//get the value of width and height for the destination image
		int width = dest.getWidth();
		int height = dest.getHeight();
		//get the source location that correspond to destination location 
		for(int row=0;row<height;row++)
		{
			for(int col=0;col<width;col++)
			{
				int x = row-height/2;
				int y = col-width/2;
				//get the value of location in polar coordinate for the destination image
				double dest_r = Math.sqrt(x*x+y*y);
				double dest_angle = Math.atan2(x, y);
				//the value of location in polar coordinate for the source image
				double src_r = 0;
				double src_angle = dest_angle;
				
				if(dest_r >= focalLength)
				{
					src_r = dest_r;
				}else if(dest_r < focalLength && flag == true)
				{
					src_r = scale * Math.log(1+weight*dest_r);
				}else{
					src_r = (Math.pow(2.718, dest_r/scale)-1) / weight;
				}
				//get the location for source image in cartesian coordinate
				int src_row = (int) (height/2 + (src_r * Math.sin(src_angle)));
				int src_col = (int) (width/2 + (src_r * Math.cos(src_angle)));
				//get samples in source image
				int red = padder.getSample(src, src_col, src_row, 0);
				int green = padder.getSample(src, src_col, src_row, 1);
				int blue = padder.getSample(src, src_col, src_row, 2);
				int samples[] = {red,green,blue};
				//set samples to the destination image
				dest.getRaster().setPixel(col, row, samples);
			}
		}
		return dest;
	}
	
	public String getAuthorName() {
		return "Chenguang Bai";
	}

	public BufferedImageOp getDefault(BufferedImage arg0) {
		return new FishLensOp();
	}

}
